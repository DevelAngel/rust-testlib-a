extern crate testlib_a;

#[test]
fn test_a_func() {
    assert_eq!(testlib_a::test_a(0), 0);
    assert_eq!(testlib_a::test_a(1), 5);
    assert_eq!(testlib_a::test_a(2), 10);
    assert_eq!(testlib_a::test_a(3), 15);
}
