pub fn test_a(a: i32) -> i32 {
    a * 5
}


#[cfg(test)]
mod tests {
    #[test]
    fn test_a() {
        assert_eq!(super::test_a(0), 0);
        assert_eq!(super::test_a(1), 5);
        assert_eq!(super::test_a(2), 10);
        assert_eq!(super::test_a(3), 15);
    }
}
